Extract users interested locations' details and reviews.
*** Note: Extractor can only extract the 5 most relevant reviews ***

## Component Configurations
### Input Mapping
    Required Columns:
    - id
    - place_name
    - street
    - city
    - state
    - country
    - zipcode
    - place_id

### Configurations
    1. Google API Key 
    2. Include Failed Request
    3. Incremental Load


Detailed configuration descriptions can be found [here](https://bitbucket.org/chanleoc/google_place_reviews).