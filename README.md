# README #
This repository is a collection of functions which allows users to fetch interested places' details and reviews.

### Introduction ###

- Google API Key
    - This component requires users to have their own Google API Access Keys. It is users' responsibilities to obtain the correct type/access rights of functional API keys to access Google Places API. For more information regarding the Access Keys, please visit [Get-API-Key]("https://developers.google.com/places/web-service/get-api-key"). On a side note, Google offers a default limit of 1,000 free requests per 24 hour period.
*WARNING: With current available Google Places API settings, users are only allowed to fetch the 5 most relevant from a single location. This will not change until Google decides to release the data to the public.* 

- Initial run
    - Component will first seek for the empty PlaceID within the rows from the input table. If PlaceID is not found, Places' address details will be used to request for PlaceID. Then, the component will export the fetched PlaceIds incrementally back to the original input table. On the other hand, if a PlaceID is found within the rows, the component will then use the PlaceID to fetch the places' reviews and details. Therefore, if the PlaceID is entered by the users, it is users' responsibilities to ensure the entered PlaceID is the location they are interested because the component will not confirm the validity of the interested location if PlaceID is entered.

- Input Mapping
    1. Data Table
        - Required Columns
            1. id
                - This "id" does not share the same meaning with the "place_id"
                - "id" is used to uniquely identify the place_name in the storage
            2. place_name
                - If place\_name is not found within the PlaceID search, component will choose from the suggested list of possible outcomes and select the closest match to replace the exisiting inputted place\_name. 
            3. street
            4. city
            5. state
            6. country
            7. zipcode
            8. place_id
                - Users can leave this column blank, component will use the rest of the details to fetch the place_id of the location

*Note: Although this component can use the best suggested address offered by Google, this component will not be able to locate the defined location if the defined address cannot be found on Google Maps. On the other hand, we also discovered there are PlaceIDs requested from Google did not work or failed upon details/reviews requests. Therefore, please be awared that there is a possibility that the request might fail given the address or placeID can be found on Google Maps.*

### Configuration ###

1. API Key
    - The manadatory google API key required for the component to access google places API
    - To obtain a Google API key: [Get-API-Key]("https://developers.google.com/places/web-service/get-api-key")
2. Include Failed Request
    - It appears that there are PlaceIDs acquired from Google do not return any information upon details/reviews request. 
    - Enable: Failed rows will be outputted to place_details file with error messages in the "rating" column; however, there will not be any outputs for the related rows in the place_reviews file.
    - Disable: Failed rows will be ignored and they will NOT be ouputted into both place_details and place_reivews file. Warning messages including error messages will be outputted into the log for the ones which failed.
3. Incremental Load
    - Loading the output tables incrementally into destinated KBC storage
    - New files will be generated if destinated output files are not found

### RAW JSON Configuration ###
```
{
    "#API_key_1": " YOUR_API_KEY",
    "include_failed_requests": true,
    "incremental": true
}
```

### Contact Info ###

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  
