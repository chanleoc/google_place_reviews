"__author__ = 'Leo Chan' & 'Mariana' & 'Martin Fisa'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'google_place_reviews'"

"""
Python 3 environment 
"""

import sys
import os
import logging
import csv
import json
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import math
import datetime
import pandas as pd
from keboola import docker
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import logging_gelf.formatters
import logging_gelf.handlers

### Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

### Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])

"""
###########################
### Sample JSON Config ####
{
    "#API_key_1": "YOUR_API_KEY",
    "include_failed_requests": true,
    "incremental": true
}
###########################
"""

### Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
API_key_1 = cfg.get_parameters()["#API_key_1"]
include_failed_requests = cfg.get_parameters()["include_failed_requests"]
incremental = cfg.get_parameters()["incremental"]

### Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
out_tables = cfg.get_expected_output_tables()
logging.info("IN tables mapped: "+str(in_tables))
logging.info("OUT tables mapped: "+str(out_tables))

### destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"

if API_key_1 == "":
    raise Exception ("Please enter your Google Places API Key.")
if len(in_tables) != 1:
    raise Exception ("Please validate your data file.")

logging.info("Output Incrementally: {0}".format(incremental))

### Setting up Retrys parameters
requesting = requests.Session()
retry = Retry(
        total=3,
        read=3,
        connect=3,
        backoff_factor=0.3,
        status_forcelist=[429],
    )
adapter = HTTPAdapter(max_retries=retry)
requesting.mount('http://', adapter)
requesting.mount('https://', adapter)

class place(object):
    def __init__(self, name, score, place_id):
        self.name = name
        self.score = score
        self.place_id = place_id

def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))
    
    return in_name

def get_output_tables(out_tables):
    """
    Evaluate output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = out_tables[0]
    in_name = table["full_path"]
    in_destination = table["source"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    return in_name

def PlaceID(row, API_key):
    #(place_name, street, city, state, country, zipcode, API_key):
    """
    Outputting a list of placeId with provided places' addresses
    """

    check_list = ["place_name", "street", "city", "state", "country", "zipcode"]
    #address = place_name + ' ' + street + ' ' + city + ' ' + country
    #address = row["place_name"]+' '+row["street"]+' '+row["city"]+' '+row["country"]
    address = ''
    for item in check_list:
        if not (str(row[item])=='nan'):
            if address == '':
                address = str(row[item])
            else:
                address = address + ' ' + str(row[item])

    query = address.replace(' ', '+')
    logging.info("place_id for {0} request parameters: {1}".format(row["place_name"], query))

    parameters = {'key': API_key, 'query': query}
    #r = requests.get('https://maps.googleapis.com/maps/api/place/textsearch/json', params = parameters)
    r = requesting.get('https://maps.googleapis.com/maps/api/place/textsearch/json', params = parameters)
    data = r.json()
    
    if data['status'] == 'ZERO_RESULTS':
        #place_id = None
        place_id = "Not Found"
        logging.warning("place_id is not found: place_name({0})".format(row["place_name"]))
        return place_id
    
    max_value = place(name="nothing", score=0, place_id="0")
    n_stores = len(data['results'])
    for i in range (0, n_stores):
        if data['results'][i]['name'] == row["place_name"]:
            place_id = data['results'][i]['place_id']
            return place_id
        else:
            ### compare outputs with google places output
            score = fuzz.partial_ratio(data['results'][i]['name'], row["place_name"])
            if score > max_value.score:
                max_value = place(name=data['results'][i]['name'], score=score, place_id=data['results'][i]['place_id'])

        if (i+1 == n_stores):
            #place_id = None
            if max_value.score >= 50:
                logging.warning("place_name is replaced with suggested place_name: {0} -> {1}".format(row["place_name"], max_value.name))
                row["place_name"] = max_value.name
                return max_value.place_id
            else:
                place_id = "Not Found"
                logging.warning("place_id is not found: place_name({0})".format(row["place_name"]))
                return place_id

def PlaceDetails(parent_id, place_id, API_key, place_name):
    """
    Requesting details of the place
    Output: place details, place reviews
    """ 

    logging.info("Processing place_id: {0}".format(place_id))
    parameters = {
        'key': API_key, 
        'placeid': place_id
    }

    #r = requests.get('https://maps.googleapis.com/maps/api/place/details/json', params = parameters)
    r = requesting.get('https://maps.googleapis.com/maps/api/place/details/json', params = parameters)
    data = r.json()
    
    if data['status']!='OK':
        empty_df = pd.DataFrame()
        if include_failed_requests: # with include_failed_request enable
            ### if request fails
            detail = {
                "id": parent_id,
                "name": place_name,        
                "place_id": place_id,
                "rating": data['status']
            }
            logging.warning("{0} ({1}): {2}".format(place_name, parent_id, data['status']))
            return detail, empty_df
        else: # with include_failed_request disable, ignoring the row
            logging.warning("{0} ({1}): {2}".format(place_name, parent_id, data['status']))
            return empty_df, empty_df

    else:
        ### if request returns data
        ### Output Google Summary of the Place
        review_df = pd.DataFrame()    
        review_list = [
            "author_name",
            "author_url",
            "language",
            "rating",
            "text",
            "time"
        ]

        if 'reviews' in data['result']:
            for item in data['result']['reviews']:
                review = {
                    "id": parent_id,
                    "name": data["result"]["name"],
                    "place_id": place_id        
                }
                for i in review_list:
                    if i in item:
                        if i is "time":
                            ### Converting the time format
                            review[i] = datetime.datetime.utcfromtimestamp(int(item[i]))
                        else:
                            review[i] = item[i]
                    else:
                        review[i] = None
                review_df = review_df.append(review, ignore_index=True)
        else:
            logging.warning("Reviews not found: place_id({0})".format(place_id))
            pass
        
        ### Output Details of the place
        detail = {
            "id": parent_id,
            "place_id": place_id        
        }
        ### list of data which the request is going to return
        detail_list = [
            "name", 
            "rating", 
            "latitude",
            "longitude",
            "formatted_address",
            "types",
            "formatted_phone_number"
        ]

        for i in detail_list:
            if i in data["result"]:
                detail[i] = data["result"][i]
            elif i is "latitude":
                detail[i] = data["result"]["geometry"]["location"]["lat"]
            elif i is "longitude":
                detail[i] = data["result"]["geometry"]["location"]["lng"]
            else:
                detail[i] = None
        #detail_df = pd.DataFrame(detail)
        #print(detail_df)

        return detail, review_df

def produce_manifest(file_name, pk):
    """
    Dummy function to return header per file type.
    """

    file = "/data/out/tables/"+str(file_name)+".csv.manifest"
    #destination_part = file_name.split(".csv")[0]

    manifest_template = {#"source": "myfile.csv"
                         #"destination": "in.c-mybucket.table"
                         #"incremental": True
                         #,"primary_key": ["VisitID","Value","MenuItem","Section"]
                         #,"columns": [""]
                         #,"delimiter": "|"
                         #,"enclosure": ""
                        }

    column_header = []

    manifest = manifest_template
    #manifest["columns"] = column_header
    #manifest["source"] = str(file_name)
    #manifest["destination"] = outputBucket+str(file_name)
    manifest["incremental"] = incremental
    #manifest["destination"] = "in.c-google_places."+file_name
    if len(pk) > 0:
        manifest["primary_key"] = pk

    try:
        with open(file, 'w') as file_out:
            json.dump(manifest, file_out)
            logging.info("Output manifest file ({0}) produced.".format(file_name))
    except Exception as e:
        logging.error("Could not produce output file manifest.")
        logging.error(e)
    
    return

def reproduce_manifest(json_text):
    """
    Reproducing an input mapping from input mapping's manifest
    """

    file_name = json_text["name"]
    file = "/data/out/tables/"+file_name+".csv.manifest"
    #destination_part = file_name.split(".csv")[0]

    manifest_template = {#"source": "myfile.csv"
                         #"destination": "in.c-mybucket.table"
                         "incremental": True
                         #,"primary_key": ["VisitID","Value","MenuItem","Section"]
                         #,"columns": [""]
                         #,"delimiter": "|"
                         #,"enclosure": ""
                        }

    column_header = []

    manifest = manifest_template
    #manifest["columns"] = column_header
    #manifest["source"] = str(file_name)
    manifest["destination"] = json_text["id"]
    manifest["primary_key"] = json_text["primary_key"]
    #manifest["columns"] = json_text["columns"]

    try:
        with open(file, 'w') as file_out:
            json.dump(manifest, file_out)
            logging.info("Output manifest: {0}".format(manifest))
            logging.info("Output manifest file ({0}) produced.".format(file_name))
    except Exception as e:
        logging.error("Could not produce output file manifest.")
        logging.error(e)
    
    return

def output(df, columns, filename, pk):
    """
    Outputting the selected dataframe file with selected columns
    """

    logging.info("Outputting {0}".format(filename))
    df.to_csv(DEFAULT_FILE_DESTINATION+filename+".csv", index=False, columns=columns)
    produce_manifest(filename, pk)

    return

def main():
    """
    Main execution script.
    """
    
    ########################################################################
    ### PART I: recycling PlaceID and call API for new PlaceIDs
    ### Getting place_id for all the places requested
    in_table = get_tables(in_tables)
    data_input = pd.read_csv(in_table, dtype=str)

    changes = 0 # Guage the number of changes in the input data table
    logging.info("Processing place_id...")
    
    for index, row in data_input.iterrows():
        if (str(row["place_id"])=='') or (str(row["place_id"])=='nan'):
            ### Case 1: if place_id column is empty, API request for the place_id
            #temp_output = 1
            #temp_output = PlaceID(row["place_name"],row["street"],row["city"],row["state"],row["country"],row["zipcode"],API_key_1)
            temp_output = PlaceID(row, API_key_1)
            row["place_id"] = temp_output
            changes += 1
        else:
            ### Case 2: if place_id column is found, do nothing
            pass

    ### When they are changes to the input mapping
    if changes > 0:
        with open(in_table+".manifest", 'r') as file:
            manifest_data = json.load(file)
            file.close()

        out_table = DEFAULT_FILE_DESTINATION+manifest_data["name"]+".csv"
        logging.info("Updating Input table: {0}".format(in_table))
        logging.info("Input table Manifest: {0}".format(manifest_data))
        logging.info("Update table Destination: {0}".format(out_table))
        reproduce_manifest(manifest_data)
        data_input.to_csv(out_table, index=False)
    #sys.exit(0)

    ########################################################################
    ### PART II: Fetching google place's details and reviews
    ### Only interested in places' review atm
    logging.info("Processing places' details and reviews...")

    place_details_column = ["id","name","place_id","rating","formatted_address","longitude","latitude","formatted_phone_number","types"]
    place_reviews_column = ["id","name","place_id","author_name","author_url","language","rating","text","time"]
    place_details = pd.DataFrame(columns=place_details_column)
    place_reviews = pd.DataFrame(columns=place_reviews_column)

    #for index, row in places.iterrows():
    places = data_input[data_input["place_id"]!="Not Found"]
    for index, row in places.iterrows():
        detail, review = PlaceDetails(row["id"], row["place_id"], API_key_1, row["place_name"])

        place_details = place_details.append(detail, ignore_index=True)
        place_reviews = place_reviews.append(review, ignore_index=True)

    place_details_pk = ["id"]
    place_reviews_pk = ["id","place_id","author_name","time"]
    if not place_details.empty:
        output(place_details, place_details_column, "place_details", place_details_pk)
    if not place_reviews.empty:
        output(place_reviews, place_reviews_column, "place_reviews", place_reviews_pk)

    return


if __name__ == "__main__":

    main()

    logging.info("Done.")
